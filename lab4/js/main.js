function initAutocomplete () {
var inputs = $('.searchTextField');
var options = {
    types: ['(regions)'],
};
$(inputs).each(function (i, element) {
    var autocomplete = new google.maps.places.Autocomplete(element, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace(); //получаем место
        console.log(place);
        console.log(place.name);  //название места
        console.log(place.id);  //уникальный идентификатор места
        });
    });
}
