$(document).ready(function(){

  let slideIndex = 1;
  let scroll = 1;

  let slide = data =>{
    slideIndex += data;
    setTimeout(showSlide, 0000, slideIndex);
  };

  let showSlide = data =>{
    let i = 1;
    const widthcontainer = 80;
    const count = 3;
    const sliderImgWidth= (widthcontainer-(count*5))/count;

    const $slides = $('.wrapImg');
    const countImagesInList = $slides.length;

    let countItemInList;
    if(count==0) countItemInList=countImagesInList;
    else countItemInList = Math.ceil(countImagesInList/count);

    $("#slider").css("width", widthcontainer+"vw");

    for(i; i<=countImagesInList; i++){
      $("#slide"+i).css("width", sliderImgWidth+"vw");
    }

    if(data>countItemInList) slideIndex = 1;
    else if(data<1) slideIndex = countItemInList;

    if(slideIndex == 1) scroll = 0;
    else scroll = -(slideIndex-1)*widthcontainer;

    $('#list').css({
               '-webkit-transform':'translateX('+scroll+'vw)'
              ,'-moz-transform':'translateX('+scroll+'vw)'
              ,'transform':'translateX('+scroll+'vw)'
    });
  };

  showSlide(slideIndex);

  $(".prev").click(function(){
    slide(-1);
  });
  $(".next").click(function(){
    slide(1);
  });
});
